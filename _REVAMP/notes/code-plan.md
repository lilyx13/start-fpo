# Start.fpo Code Plan

## Side card issue
- aside cards will be difficult to make work on mobile. JS will be needed to adjust the layout at a 1 breakpoint
	- Documentation links condensed into the mobile menu
	- Latest solved issues become small tiles under search (columns)
	- Articles in cards, 1/3 - 1/4 of the screen height per card
		- reduce number of cards shown (3, 2)

### FIX
- Go simple, make the documentation and solved issues be two links underneath the search bar

## Search Bar
- Use browser default search bar

## nav issues
- mobile/ small screen nav is meant to appea to both mobile screen and 1/2 to 1/4 screen size.
	- mobile use case:
		- exploration
		- reading
		- having broken computer and panicing
	- small screen on desktop use case:
		- working on multiple things
			- potentially debugging an issue and getting to docs
			- emotional/ experiential bonus points for a pleasing and quick experience, so people can get to what they need right away.
		- editing configuration files, possibly on just 1 screen.
	- landscape tablet to full desktop use case:
		- first opening a browser or tab
		- widest possible search use case
	- what this page should always offer
		- a sense of home base, the way that a lot of people experience opening chrome and seeing that ui
		- Not completely empty, the content should be useful and interesting -> Spark curiosity for new users (fedora) offer updates on what's going on in the fedora sphere

## Components
- Articles should either be full width cards at mobile or in a gallery, but based on everything else that I've looked at wtih fedora, keeping it single column will likely fly better

## Interaction
- UI intections shouldn't be too in your face
- big buttons aren't so needed or common yet, if they are to be a thing, it won't be with this design.
	- instead, use well defined text sizes, color, hover, and weight for links. 
- use 1 hover design, lighten on hover
	- a sense of illuminating things
